#ifndef MOBIUSWORLD_H
#define MOBIUSWORLD_H

#include "model/World.h"
#include "model/mat4.h"
#include "model/Camera.h"
#include "model/Light.h"
#include "model/Torus.h"
#include "model/Sphere.h"
#include "Mobius.h"
#include "Drawer.h"
#include "RollingTorus.h"
#include "CheckerBoardTexture.h"
#include "SkyTexture.h"

class MobiusWorld : public World {
    Drawer mobiusDrawer, skyDrawer;
    Mobius mobius;
    std::vector<RollingTorus*> toruses;
    Sphere sky;

    mat4 v, p;

    Camera camera;
    Light light;
    
    CheckerBoardTexture mobiusTexture, torusTexture;
    SkyTexture skyTexture;

    bool birdsEyeView;
    bool cameraLeft;

    GLfloat t;

    const static GLfloat BIRDS_EYE_DISTANCE;
    const static GLfloat CAMERA_SPEED;
    const static int NUMBER_OF_TORUSES;
    const static unsigned int RANDOM_SEED;

public:
    MobiusWorld(ShaderProgram& program);
    ~MobiusWorld();

    void onDraw() override;
    void onInitialize() override;

    void transformM(const mat4& m, const mat4& mInv);

    void onAnimate(GLfloat t, GLfloat dt);

    GLfloat time() const;

    void toggleView();
    void stepLeft();
    void stepRight();

    void getMobiusPosition(mat4& m, mat4& mInv, GLfloat u, GLfloat v,
            GLfloat height) const;

    void setAspectRatio(GLfloat asp);

private:
    void generateToruses();
    void destroyToruses();
    void setBirdsEyeView();
};

#endif /* MOBIUSWORLD_H */

