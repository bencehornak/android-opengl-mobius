#include "CheckerBoardTexture.h"
#include <GLES3/gl3.h>

CheckerBoardTexture::CheckerBoardTexture(ShaderProgram& program,
        const char* samplerName, GLuint textureUnit,
        const int width, const int height)
: Texture(program, samplerName, textureUnit), width(width), height(height) {
}

void CheckerBoardTexture::onUploadTexture() {
    std::vector<vec3> image(width * height);
    const vec3 yellow(1, 1, 0), blue(0, 0, 1);
    for (int x = 0; x < width; x++) for (int y = 0; y < height; y++) {
            image[y * width + x] = (x ^ y) & 1 ? yellow : blue;
        }
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, width, height, 0, GL_RGB, GL_FLOAT,
            image.data()); //Texture->OpenGL
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
}
