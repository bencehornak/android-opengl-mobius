#ifndef MOBIUSPROGRAM_HPP
#define MOBIUSPROGRAM_HPP

#include "gl/ShaderProgram.h"
#include "MobiusWorld.h"

class MobiusProgram : public ShaderProgram {
    MobiusWorld world;
    Shader vertexShader, fragmentShader;
public:
    MobiusProgram();

    void onBeforeLink() override;

    void onAnimate(GLfloat t, GLfloat dt);
    
    void toggleView();
    void stepLeft();
    void stepRight();

    void setCameraSize(int width, int height);
};

#endif /* MOBIUSPROGRAM_HPP */

