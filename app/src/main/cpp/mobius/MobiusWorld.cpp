#include "MobiusWorld.h"
#include "gl/ShaderProgram.h"

#define _USE_MATH_DEFINES
#include <cmath>
#include <vector>
#include "Material.h"

const GLfloat MobiusWorld::BIRDS_EYE_DISTANCE = 10;
const GLfloat MobiusWorld::CAMERA_SPEED = -.1;
const int MobiusWorld::NUMBER_OF_TORUSES = 5;
const unsigned int MobiusWorld::RANDOM_SEED = 92873828;

const Material PLAIN(vec3(.4, .5, .5), vec3(0.1, 0.1, 0.3), vec3(.2, .2, .2), 10);
const Material SKY(vec3(), vec3(), vec3(1, 1, 1), 0);

MobiusWorld::MobiusWorld(ShaderProgram& program)
: World(program), mobiusDrawer(*this, PLAIN), skyDrawer(*this, SKY),
mobius(&mobiusTexture, mobiusDrawer, 4, .7), sky(&skyTexture, skyDrawer, 12),
camera(M_PI / 3, 1, .1, 25),
light(vec3(1, 1, 1), vec3(3, 3, 3), vec4(5, 5, 4, 0)),
mobiusTexture(program, "diffuseTexture", 0, 10, 10),
torusTexture(program, "diffuseTexture", 1, 10, 10),
skyTexture(program, "diffuseTexture", 2, 1000, 1000, .001),
birdsEyeView(true), cameraLeft(true), t(0) {

    objects.push_back(&sky);
    objects.push_back(&mobius);
    srand(RANDOM_SEED);
    generateToruses();
}

MobiusWorld::~MobiusWorld() {
    destroyToruses();
}

void MobiusWorld::onInitialize() {

    World::onInitialize();
}

void MobiusWorld::onDraw() {

    v = camera.transformV();
    p = camera.transformP();

    program.setUniform("wEye", camera.wEye);

    program.setUniform("La", light.La);
    program.setUniform("Le", light.Le);
    program.setUniform("wLiPos", light.wLightPos);
    World::onDraw();
}

void MobiusWorld::transformM(const mat4& m, const mat4& mInv) {
    program.setUniform("M", m);
    program.setUniform("Minv", mInv);
    program.setUniform("MVP", m * v * p);
}

void MobiusWorld::onAnimate(GLfloat _t, GLfloat dt) {
    t = _t;
    (void) dt;

    if (birdsEyeView)
        setBirdsEyeView();
    else
        mobius.setCamera(camera, cameraLeft ? 0.25 : 0.75, CAMERA_SPEED * t, .2);
}

GLfloat MobiusWorld::time() const {
    return t;
}

void MobiusWorld::setBirdsEyeView() {
    camera.wEye = vec3(0, 1, 0) + (vec4(0, 0, 1) * mat4::rotateY(.5 * time()) * BIRDS_EYE_DISTANCE);
    camera.wLookat = vec3();
    camera.wVup = vec3(0, 1, 0);
}

void MobiusWorld::toggleView() {
    birdsEyeView = !birdsEyeView;
}

void MobiusWorld::stepLeft() {
    cameraLeft = true;
}

void MobiusWorld::stepRight() {
    cameraLeft = false;
}

void MobiusWorld::getMobiusPosition(mat4& m, mat4& mInv, GLfloat u, GLfloat v, GLfloat height) const {
    mobius.getRollingPosition(m, mInv, u, v, height);
}

void MobiusWorld::generateToruses() {
    for (int i = 0; i < NUMBER_OF_TORUSES; i++) {
        toruses.push_back(new RollingTorus(*this, &torusTexture, PLAIN,
                rand() % 2,
                (GLfloat) i / NUMBER_OF_TORUSES - 1));
        objects.push_back(&toruses[i]->torus);
    }

}

void MobiusWorld::destroyToruses() {
    for (int i = 0; i < toruses.size(); ++i)
        delete toruses[i];
    toruses.clear();
}

void MobiusWorld::setAspectRatio(GLfloat asp) {
    camera.asp = asp;
}
