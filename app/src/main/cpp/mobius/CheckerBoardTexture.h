#ifndef CHECKERBOARDTEXTURE_H
#define CHECKERBOARDTEXTURE_H

#include "gl/ShaderProgram.h"
#include "model/Texture.h"

class CheckerBoardTexture : public Texture {
    const int width, height;
public:
    CheckerBoardTexture(ShaderProgram& program, const char* samplerName, 
            GLuint textureUnit, const int width, const int height);

    void onUploadTexture() override;

};

#endif /* CHECKERBOARDTEXTURE_H */

