#ifndef MATERIAL_H
#define MATERIAL_H

#include <GLES3/gl3.h>
#include "model/vec3.h"

class Material {
public:
    const vec3 kd, ks, ka;
    GLfloat shine;
    Material(vec3 kd, vec3 ks, vec3 ka, GLfloat shine);

};

#endif /* MATERIAL_H */

