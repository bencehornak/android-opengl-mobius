#ifndef DRAWER_H
#define DRAWER_H

#include "model/ParamSurface.h"
#include "model/mat4.h"
#include "Material.h"

class MobiusWorld;

class Drawer : public ParamSurfaceDrawer {
    MobiusWorld& world;
    const Material& material;
    GLuint vao;
    GLuint vbo;

public:
    Drawer(MobiusWorld& world, const Material& material);

    void onInitialize() override;
    void uploadTriangles(int numVertices, VertexData* triangleGrid) override;
    void drawTriangles(int numVertices) override;

    virtual void getTransformM(mat4& m, mat4& mInv);
};

#endif /* DRAWER_H */

