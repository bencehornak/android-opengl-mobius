#include "SkyTexture.h"
#include <vector>
#include <cstdlib>
#include "model/vec3.h"

SkyTexture::SkyTexture(ShaderProgram& program, const char* samplerName,
        GLuint textureUnit, int width, int height, float starProbability)
: Texture(program, samplerName, textureUnit), width(width), height(height),
starProbability(starProbability) {
}

void SkyTexture::onUploadTexture() {
    std::vector<vec3> image(width * height);
    const vec3 dark(.01, .01, .01), star(.9, .9, .9);
    for (int x = 0; x < width; x++)
        for (int y = 0; y < height; y++)
            image[y * width + x] = rand() < starProbability * RAND_MAX ? star : dark;
    
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, width, height, 0, GL_RGB, GL_FLOAT,
            image.data()); //Texture->OpenGL
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
}



