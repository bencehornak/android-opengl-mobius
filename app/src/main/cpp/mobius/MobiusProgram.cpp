#include "MobiusProgram.h"
#include "gl/Shader.h"
#include "model/ParamSurface.h"

const char* VERTEX_SHADER_SOURCE =
        "#version 300 es\n"
        "precision highp float;\n"
        "\n"
        "uniform mat4  MVP, M, Minv; // MVP, Model, Model-inverse\n"
        "uniform vec4  wLiPos;       // light source direction \n"
        "uniform vec3  wEye;         // pos of eye\n"
        "\n"
        "layout(location = 0) in vec3  vtxPos;            // pos in modeling space\n"
        "layout(location = 1) in vec3  vtxNorm;      	 // normal in modeling space\n"
        "layout(location = 2) in vec2  vtxUV;\n"
        "\n"
        "out vec3 wNormal;		    // normal in world space\n"
        "out vec3 wView;             // view in world space\n"
        "out vec3 wLight;		    // light dir in world space\n"
        "out vec2 texcoord;\n"
        "\n"
        "void main() {\n"
        "    gl_Position = vec4(vtxPos, 1) * MVP; // to NDC\n"
        "    // vectors for radiance computation\n"
        "    vec4 wPos = vec4(vtxPos, 1) * M;\n"
        "    wLight = wLiPos.xyz * wPos.w - wPos.xyz * wLiPos.w;\n"
        "    wView  = wEye * wPos.w - wPos.xyz;\n"
        "    wNormal = (Minv * vec4(vtxNorm, 0)).xyz;\n"

        "    texcoord = vtxUV;\n"
        "}\n";
const char* FRAGMENT_SHADER_SOURCE =
        "#version 300 es\n"
        "precision highp float;\n"

        "uniform vec3 kd, ks, ka; // diffuse, specular, ambient ref\n"
        "uniform vec3 La, Le;     // ambient and point sources\n"
        "uniform float shine;     // shininess for specular ref\n"
        "uniform sampler2D diffuseTexture;\n"
        "\n"
        "in  vec3 wNormal;       // interpolated world sp normal\n"
        "in  vec3 wView;         // interpolated world sp view\n"
        "in  vec3 wLight;        // interpolated world sp illum dir\n"
        "in vec2 texcoord;\n"
        "out vec4 fragmentColor; // output goes to frame buffer\n"
        "\n"
        "void main() {\n"
        "    vec3 N = normalize(wNormal);\n"
        "    vec3 V = normalize(wView); \n"
        "    vec3 L = normalize(wLight);\n"
        "    vec3 H = normalize(L + V);\n"
        "    float cost = abs(dot(N,L)), cosd = abs(dot(N,H));\n"
        "    vec3 texColor = texture(diffuseTexture, texcoord).rgb;\n"

        "    // kd and ka are modulated by the texture\n"
        "    vec3 color = ka * texColor * La + (kd * texColor * cost + ks * pow(cosd,shine)) * Le;\n"
        "    fragmentColor = vec4(color, 1);\n"
        "}\n";

MobiusProgram::MobiusProgram()
: ShaderProgram(world), world(*this), vertexShader(GL_VERTEX_SHADER, VERTEX_SHADER_SOURCE),
fragmentShader(GL_FRAGMENT_SHADER, FRAGMENT_SHADER_SOURCE) {

    shaders.push_back(&vertexShader);
    shaders.push_back(&fragmentShader);
}

void MobiusProgram::onBeforeLink() {
    // TODO glBindFragDataLocation(program, 0, "fragmentColor");
}

void MobiusProgram::onAnimate(GLfloat t, GLfloat dt) {
    world.onAnimate(t, dt);
}

void MobiusProgram::toggleView() {
    world.toggleView();
}

void MobiusProgram::stepLeft() {
    world.stepLeft();
}

void MobiusProgram::stepRight() {
    world.stepRight();
}

void MobiusProgram::setCameraSize(int width, int height) {
    world.setAspectRatio((GLfloat)width / height);
}

