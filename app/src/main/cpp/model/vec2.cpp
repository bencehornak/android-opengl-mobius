#define _USE_MATH_DEFINES
#include <cmath>

#include "vec2.h"

vec2::vec2() {
}

vec2::vec2(GLfloat x, GLfloat y)
: x(x), y(y) {
}

GLfloat vec2::norm() const {
    return sqrtf((*this) * (*this));
}

vec2 vec2::normalize() const {
    return (*this) / norm();
}

vec2 vec2::operator+(const vec2& v) const {
    return vec2(x + v.x, y + v.y);
}

vec2 vec2::operator-(const vec2& v) const {
    return vec2(x - v.x, y - v.y);
}

vec2 vec2::operator*(GLfloat l) const {
    return vec2(l*x, l * y);
}

vec2 vec2::operator/(GLfloat l) const {
    return vec2(x / l, y / l);
}

GLfloat vec2::operator*(const vec2& v) const {
    return x * v.x + y * v.y;
}