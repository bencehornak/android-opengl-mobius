#ifndef PARAMSURFACE_H
#define PARAMSURFACE_H

#include <vector>
#include <GLES3/gl3.h>
#include "vec3.h"
#include "vec2.h"
#include "gl/Initializable.h"
#include "Drawable.h"

class VertexData {
public:
    vec3 position, normal;
    vec2 texcoord;

    VertexData();
    VertexData(vec3& position, vec3& normal, vec2& texcoord);

};

class ParamSurfaceDrawer : public Initializable {
public:
    virtual ~ParamSurfaceDrawer();
    virtual void uploadTriangles(int numVertices, VertexData* triangleGrid) = 0;
    virtual void drawTriangles(int numVertices) = 0;
};

class ParamSurface : public Drawable {
    ParamSurfaceDrawer& drawer;
    int n, m;
    int numVertices;
    int holeSize, period;
public:
    ParamSurface(Texture* texture, ParamSurfaceDrawer& drawer, int n, int m,
            int holeSize = 0, int period = 1);
    ParamSurface(const ParamSurface& orig) = delete;
    ParamSurface& operator=(const ParamSurface& right) = delete;
    virtual ~ParamSurface();

    void createTriangles();

protected:
    void onInitialize() override;
    void onDraw() override;
    
    virtual VertexData generateVertexData(GLfloat u, GLfloat v) = 0;

private:
    void generateAllVertexData(int n, int m, VertexData* dest);
    void generateTriangleGrid(int n, int m, std::vector<VertexData>& dest,
            VertexData* vertices);

};

#endif /* PARAMSURFACE_H */

