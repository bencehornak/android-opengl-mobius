#ifndef DRAWABLE_H
#define DRAWABLE_H

#include <GLES3/gl3.h>
#include "gl/Initializable.h"
#include "Texture.h"

class Drawable : public Initializable {
    Texture* texture;
public:
    Drawable(Texture* texture);
    Drawable(const Drawable& orig) = delete;
    Drawable& operator=(const Drawable& right) = delete;

    virtual ~Drawable();

    void draw();

protected:
    virtual void onDraw();
    void onInitialize() override;

};

#endif /* DRAWABLE_H */