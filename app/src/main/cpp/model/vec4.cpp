#include "vec4.h"

vec4::vec4(float _x, float _y, float _z, float _w) {
    x = _x;
    y = _y;
    z = _z;
    w = _w;
}

vec4::vec4(const vec3& v)
: vec4(v.x, v.y, v.z, 1) {
}

GLfloat vec4::dx() const {
    return x / w;
}

GLfloat vec4::dy() const {
    return y / w;
}

GLfloat vec4::dz() const {
    return z / w;
}

vec4 vec4::operator*(const mat4& mat) const {
    return vec4(x * mat.m[0][0] + y * mat.m[1][0] + z * mat.m[2][0] + w * mat.m[3][0],
            x * mat.m[0][1] + y * mat.m[1][1] + z * mat.m[2][1] + w * mat.m[3][1],
            x * mat.m[0][2] + y * mat.m[1][2] + z * mat.m[2][2] + w * mat.m[3][2],
            x * mat.m[0][3] + y * mat.m[1][3] + z * mat.m[2][3] + w * mat.m[3][3]);
}

GLfloat vec4::operator*(const vec4& v) const {
    return x * v.x + y * v.y + z * v.z + w * v.w;
}

vec4 vec4::operator*(GLfloat lambda) const {
    return vec4(x*lambda, y*lambda, z*lambda, w);
}

vec4::operator vec3() const {
    return vec3(dx(), dy(), dz());
}
