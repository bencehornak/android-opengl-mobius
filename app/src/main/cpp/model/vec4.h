#ifndef VEC4_H
#define VEC4_H

#include <GLES3/gl3.h>

#include "mat4.h"
#include "vec3.h"

// 3D point in homogeneous coordinates

struct vec4 {
    GLfloat x, y, z, w;

    vec4(float _x = 0, float _y = 0, float _z = 0, float _w = 1);
    vec4(const vec3& v);

    GLfloat dx() const;
    GLfloat dy() const;
    GLfloat dz() const;

    vec4 operator*(const mat4& mat) const;
    GLfloat operator*(const vec4& v) const;
    vec4 operator*(GLfloat lambda) const;
    
    operator vec3() const;
};

#endif /* VEC4_H */

