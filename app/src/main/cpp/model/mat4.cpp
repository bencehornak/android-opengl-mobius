#include <cmath>
#include "mat4.h"
#include "vec4.h"

mat4::mat4() {
}

mat4::mat4(float m00, float m01, float m02, float m03,
        float m10, float m11, float m12, float m13,
        float m20, float m21, float m22, float m23,
        float m30, float m31, float m32, float m33) {
    m[0][0] = m00;
    m[0][1] = m01;
    m[0][2] = m02;
    m[0][3] = m03;
    m[1][0] = m10;
    m[1][1] = m11;
    m[1][2] = m12;
    m[1][3] = m13;
    m[2][0] = m20;
    m[2][1] = m21;
    m[2][2] = m22;
    m[2][3] = m23;
    m[3][0] = m30;
    m[3][1] = m31;
    m[3][2] = m32;
    m[3][3] = m33;
}

mat4 mat4::operator*(const mat4& right) const {
    mat4 result;
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            result.m[i][j] = 0;
            for (int k = 0; k < 4; k++) result.m[i][j] += m[i][k] * right.m[k][j];
        }
    }
    return result;
}

mat4::operator GLfloat*() {
    return &m[0][0];
}

mat4::operator const GLfloat*() const {
    return &m[0][0];
}

/** Returns identity matrix */
mat4 mat4::identity() {
    return mat4(
            1, 0, 0, 0,
            0, 1, 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1
            );
}

/** Returns the matrix of rotating around z axis */
mat4 mat4::rotateX(GLfloat phi) {
    float sin = sinf(phi), cos = cosf(phi);
    return mat4(
            1, 0, 0, 0,
            0, cos, sin, 0,
            0, -sin, cos, 0,
            0, 0, 0, 1
            );
}

/** Returns the matrix of rotating around y axis */
mat4 mat4::rotateY(GLfloat phi) {
    float sin = sinf(phi), cos = cosf(phi);
    return mat4(
            cos, 0, sin, 0,
            0, 1, 0, 0,
            -sin, 0, cos, 0,
            0, 0, 0, 1
            );
}

/** Returns the matrix of rotating around z axis */
mat4 mat4::rotateZ(GLfloat phi) {
    float sin = sinf(phi), cos = cosf(phi);
    return mat4(
            cos, sin, 0, 0,
            -sin, cos, 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1
            );
}

/** Returns a translation matrix */
mat4 mat4::translate(GLfloat tx, GLfloat ty, GLfloat tz) {
    return mat4(
            1, 0, 0, 0,
            0, 1, 0, 0,
            0, 0, 1, 0,
            tx, ty, tz, 1
            );
}

mat4 mat4::translate(const vec3& t) {
    return translate(t.x, t.y, t.z);
}

mat4 mat4::scale(GLfloat sx, GLfloat sy, GLfloat sz) {
    return mat4(
            sx, 0, 0, 0,
            0, sy, 0, 0,
            0, 0, sz, 0,
            0, 0, 0, 1
            );
}

/** x -> rx, y -> ry, z -> rz */
mat4 mat4::rotate(const vec4& rx, const vec4& ry, const vec4& rz) {
    return mat4(
            rx.dx(), rx.dy(), rx.dz(), 0,
            ry.dx(), ry.dy(), ry.dz(), 0,
            rz.dx(), rz.dy(), rz.dz(), 0,
            0, 0, 0, 1
            );
}