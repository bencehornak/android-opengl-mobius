#ifndef TEXTURE_H
#define TEXTURE_H

#include <GLES3/gl3.h>
#include "gl/Initializable.h"

class ShaderProgram;

class Texture : public Initializable {
    ShaderProgram& program;
    const char* samplerName;
    GLuint textureId;
    GLuint textureUnit;
public:
    Texture(ShaderProgram& program, const char* samplerName, GLuint textureUnit);
    virtual ~Texture();

    void onInitialize() override;
    virtual void onUploadTexture() = 0;
    void bind();

};

#endif /* TEXTURE_H */

