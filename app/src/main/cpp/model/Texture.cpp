#include "Texture.h"
#include "gl/ShaderProgram.h"

Texture::Texture(ShaderProgram& program, const char* samplerName, GLuint textureUnit)
: program(program), samplerName(samplerName), textureUnit(textureUnit) {
}

Texture::~Texture() {
}

void Texture::onInitialize() {
    glGenTextures(1, &textureId);
    bind();
    onUploadTexture();
}

void Texture::bind() {
    program.setUniform(samplerName, (GLint) textureUnit);
    glActiveTexture(GL_TEXTURE0 + textureUnit);
    glBindTexture(GL_TEXTURE_2D, textureId);
}
