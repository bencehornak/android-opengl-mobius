#ifndef VEC2_H
#define VEC2_H

#include <GLES3/gl3.h>

class vec2 {
public:
    GLfloat x, y;
    vec2();
    vec2(GLfloat x, GLfloat y);

    GLfloat norm() const;
    vec2 normalize() const;

    vec2 operator+(const vec2& v) const;
    vec2 operator-(const vec2& v) const;
    vec2 operator*(GLfloat lambda) const;
    vec2 operator/(GLfloat lambda) const;
    GLfloat operator*(const vec2& v) const;

};

#endif /* VEC2_H */

