#ifndef MAT4_H
#define MAT4_H

#include "GLES3/gl3.h"
#include "vec3.h"

struct vec4; // Circular dependency

struct mat4 {
    float m[4][4];
public:

    mat4();

    mat4(float m00, float m01, float m02, float m03,
            float m10, float m11, float m12, float m13,
            float m20, float m21, float m22, float m23,
            float m30, float m31, float m32, float m33);

    mat4 operator*(const mat4& right) const;

    operator GLfloat*();
    operator const GLfloat*() const;

    /** Returns identity matrix */
    static mat4 identity();

    /** Returns the matrix of rotating around z axis */
    static mat4 rotateX(GLfloat phi);

    /** Returns the matrix of rotating around y axis */
    static mat4 rotateY(GLfloat phi);

    /** Returns the matrix of rotating around z axis */
    static mat4 rotateZ(GLfloat phi);

    /** Returns a translation matrix */
    static mat4 translate(GLfloat tx, GLfloat ty, GLfloat tz = 0);

    static mat4 translate(const vec3& t);

    static mat4 scale(GLfloat sx, GLfloat sy, GLfloat sz = 0);

    /** x -> rx, y -> ry, z -> rz */
    static mat4 rotate(const vec4& rx, const vec4& ry, const vec4& rz);
};


#endif /* MAT4_H */

