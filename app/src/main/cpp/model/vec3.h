#ifndef VEC3_H
#define VEC3_H

#include <GLES3/gl3.h>

class vec3 {
public:
    GLfloat x, y, z;
    vec3();
    vec3(GLfloat x, GLfloat y, GLfloat z);

    GLfloat norm() const;
    vec3 normalize() const;

    vec3 operator-() const;
    vec3 operator+(const vec3& v) const;
    vec3 operator-(const vec3& v) const;
    vec3 operator*(GLfloat lambda) const;
    vec3 operator/(GLfloat lambda) const;
    GLfloat operator*(const vec3& v) const;

    vec3 cross(vec3& v) const;

};

#endif /* VEC3_H */

