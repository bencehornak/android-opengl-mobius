#include <GLES3/gl3.h>
#include <android/log.h>
#include "mobius/MobiusProgram.h"
#include <string>
#include <algorithm>
#include <cinttypes>
#include <jni.h>

const char *LOG_TAG = "Mobius JNI";

MobiusProgram *program = nullptr;

extern "C"
JNIEXPORT void JNICALL Java_hu_hb_openglmobius_NativeLibrary_onInitialization
        (JNIEnv *, jclass) {
    __android_log_write(ANDROID_LOG_INFO, LOG_TAG, "onInit");
    glViewport(0, 0, 100, 100); // TODO
    glEnable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);

    try {
        if(program)
            delete program;

        program = new MobiusProgram();
        program->init();
    } catch (const char *msg) {
        __android_log_print(ANDROID_LOG_FATAL, LOG_TAG, "Exception: %s", msg);
    } catch (std::string msg) {
        __android_log_print(ANDROID_LOG_FATAL, LOG_TAG, "Exception: %s", msg.c_str());
    } catch (...) {
        __android_log_write(ANDROID_LOG_FATAL, LOG_TAG, "Exception");
    }
}

extern "C"
JNIEXPORT void JNICALL Java_hu_hb_openglmobius_NativeLibrary_onDisplay
        (JNIEnv *, jclass) {
    glClearColor(0, 0, 0, 1); // background color
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the screen

    program->draw();

    // TODO glutSwapBuffers(); // exchange the two buffers
}

extern "C"
JNIEXPORT void JNICALL Java_hu_hb_openglmobius_NativeLibrary_onIdle(JNIEnv *env, jclass type,
                                                                    jlong time) {

    static jlong tinit = 0;
    static GLfloat tend = 0;
    if (tinit == 0)
        tinit = time;

    const GLfloat dt = .1f; // dt is infinitesimal
    GLfloat tstart = tend;
    tend = (time - tinit) / 1000.0f;

    for (GLfloat t = tstart; t < tend; t += dt) {
        float Dt = std::min(dt, tend - t);
        program->onAnimate(t, Dt);
    }
}
extern "C"
JNIEXPORT void JNICALL
Java_hu_hb_openglmobius_NativeLibrary_onSurfaceChanged(JNIEnv *env, jclass type, jint width,
                                                       jint height) {

    glViewport(0, 0, width, height);
    program->setCameraSize(width, height);
}

extern "C"
JNIEXPORT void JNICALL
Java_hu_hb_openglmobius_NativeLibrary_toggleView(JNIEnv *env, jclass type) {

    program->toggleView();

}

extern "C"
JNIEXPORT void JNICALL
Java_hu_hb_openglmobius_NativeLibrary_stepLeft(JNIEnv *env, jclass type) {
    program->stepLeft();
}

extern "C"
JNIEXPORT void JNICALL
Java_hu_hb_openglmobius_NativeLibrary_stepRight(JNIEnv *env, jclass type) {
    program->stepRight();
}