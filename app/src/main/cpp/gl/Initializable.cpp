#include "Initializable.h"

Initializable::Initializable() : inited(false) {
}

Initializable::~Initializable() {
}


void Initializable::init() {
    if(inited)
        return;
    inited = true;
    onInitialize();
}