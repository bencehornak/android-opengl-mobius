#ifndef SHADERPROGRAM_H
#define SHADERPROGRAM_H

#include <GLES3/gl3.h>
#include <vector>

#include "Shader.h"
#include "model/World.h"
#include "model/mat4.h"
#include "model/vec2.h"
#include "model/vec3.h"
#include "model/vec4.h"

class ShaderProgram : public Initializable {
protected:
    GLuint program;
    std::vector<Shader*> shaders;

    World& world;
public:
    ShaderProgram(World& world);
    ShaderProgram(const ShaderProgram& other) = delete;
    ShaderProgram& operator=(const ShaderProgram& right) = delete;

    virtual ~ShaderProgram();
    void use();
    void draw();
    GLint getUniformLocation(const char* fieldName) const;
    void setUniform(const char* name, GLint i);
    void setUniform(const char* name, GLfloat f);
    void setUniform(const char* name, const vec2& v);
    void setUniform(const char* name, const vec3& v);
    void setUniform(const char* name, const vec4& v);
    void setUniform(const char* name, const mat4& mat);

protected:
    void onInitialize() override;
    virtual void onBeforeLink();
    virtual void onDraw();
};

#endif /* SHADERPROGRAM_H */

