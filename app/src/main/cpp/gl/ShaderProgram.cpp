#include <cstdio>
#include <cstdlib>

#include "glsl_check.h"
#include "ShaderProgram.h"

ShaderProgram::ShaderProgram(World& world) : world(world) {
}

ShaderProgram::~ShaderProgram() {
    glDeleteProgram(program);
}

void ShaderProgram::onInitialize() {

    // Attach shaders to a single program
    program = glCreateProgram();
    if (!program) {
        printf("Error in shader program creation\n");
        exit(1);
    }

    for (Shader* shader : shaders) {
        shader->init();
        glAttachShader(program, shader->ref());
    }

    onBeforeLink();
    // program packaging
    glLinkProgram(program);
    checkLinking(program);

    use();

    world.init();
}

void ShaderProgram::draw() {
    use();
    onDraw();
}

void ShaderProgram::onDraw() {
    for (Shader* shader : shaders)
        shader->draw();
    world.draw();
}

void ShaderProgram::onBeforeLink() {
}

void ShaderProgram::use() {
    glUseProgram(program);
}

GLint ShaderProgram::getUniformLocation(const char* fieldName) const {
    GLint ret = glGetUniformLocation(program, fieldName);
    if (ret < 0)
        throw "Uniform not found";
    return ret;
}

void ShaderProgram::setUniform(const char* name, GLint i) {
    glUniform1i(getUniformLocation(name), i);
}

void ShaderProgram::setUniform(const char* name, GLfloat f) {
    glUniform1f(getUniformLocation(name), f);
}

void ShaderProgram::setUniform(const char* name, const vec2& v) {
    glUniform2f(getUniformLocation(name), v.x, v.y);
}

void ShaderProgram::setUniform(const char* name, const vec3& v) {
    glUniform3f(getUniformLocation(name), v.x, v.y, v.z);
}

void ShaderProgram::setUniform(const char* name, const vec4& v) {
    glUniform4f(getUniformLocation(name), v.x, v.y, v.z, v.w);
}

void ShaderProgram::setUniform(const char* name, const mat4& mat) {
    glUniformMatrix4fv(getUniformLocation(name), 1, GL_TRUE, (const GLfloat*) mat);
}