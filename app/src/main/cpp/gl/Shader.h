#ifndef SHADER_H
#define SHADER_H

#include <GLES3/gl3.h>
#include "Initializable.h"

class Shader : public Initializable {
    GLenum type;
    GLuint _ref;
    const char * source;

public:
    Shader(GLenum type, const char * source);
    Shader(const Shader& copy) = delete;
    Shader& operator=(const Shader& right) = delete;
    GLuint ref() const;
    void draw();
protected:
    void onInitialize() override;
    virtual void onDraw();
};

#endif /* SHADER_H */

