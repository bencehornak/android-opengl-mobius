#ifndef INITIALIZABLE_H
#define INITIALIZABLE_H

class Initializable {
    bool inited;
public:
    Initializable();
    virtual ~Initializable();
    void init();
protected:
    virtual void onInitialize() = 0;

};

#endif /* INITIALIZABLE_H */

