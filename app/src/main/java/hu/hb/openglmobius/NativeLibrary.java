package hu.hb.openglmobius;

public class NativeLibrary {

    static {
        System.loadLibrary("native-lib");
    }

    public static native void onInitialization();
    public static native void onDisplay();
    public static native void onIdle(long time);
    public static native void onSurfaceChanged(int width, int height);
    public static native void toggleView();
    public static native void stepLeft();
    public static native void stepRight();
}
