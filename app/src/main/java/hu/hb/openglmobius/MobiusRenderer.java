package hu.hb.openglmobius;

import android.opengl.GLES30;
import android.opengl.GLSurfaceView;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class MobiusRenderer implements GLSurfaceView.Renderer {
    @Override
    public void onSurfaceCreated(GL10 gl10, EGLConfig eglConfig) {
        NativeLibrary.onInitialization();
    }

    @Override
    public void onSurfaceChanged(GL10 gl10, int width, int height) {
        NativeLibrary.onSurfaceChanged(width, height);
    }

    @Override
    public void onDrawFrame(GL10 gl10) {
        NativeLibrary.onIdle(System.currentTimeMillis());
        NativeLibrary.onDisplay();
    }
}
